from typing import List, Union

import numpy as np
import cv2
import math


def segment_rotate_image(img: np.ndarray, pts: List[List[int]], dtype='float32') -> np.ndarray:
    """
    Segments an image and rotate the resulting segment according to the given quadrilateral coordinates

    :param img: Input image as NumPy array
    :param pts: List of anchors to segment, in the order [top_left, top_right, bottom_right, bottom_left]

    :return: The segmented and aligned image
    """
    pts = np.asarray(pts, dtype=dtype)

    (tl, tr, br, bl) = pts

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth, 0],
        [maxWidth, maxHeight],
        [0, maxHeight]], dtype=dtype
    )

    M = cv2.getPerspectiveTransform(pts, dst)
    warped = cv2.warpPerspective(img, M, (maxWidth, maxHeight))

    return warped


def transform_segmented_pts(proj_pts: Union[List, np.ndarray], target_pts: Union[List, np.ndarray], dtype='float32'):
    """
    Transforms a quadrilateral specified on a segmented image into its corresponding coordinates on the original image
    
    :param proj_pts: Coordinates of the segmented image on the original image
    :param target_pts: Coordinates of the quadrilateral within the segmented image
    
    :return: Projected coordinates of target_pts on the original image
    """
    proj_pts = np.asarray(proj_pts, dtype=dtype)
    proj_tl, proj_tr, proj_br, proj_bl = proj_pts

    widthA = np.sqrt(((proj_br[0] - proj_bl[0]) ** 2) + ((proj_br[1] - proj_bl[1]) ** 2))
    widthB = np.sqrt(((proj_tr[0] - proj_tl[0]) ** 2) + ((proj_tr[1] - proj_tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((proj_tr[0] - proj_br[0]) ** 2) + ((proj_tr[1] - proj_br[1]) ** 2))
    heightB = np.sqrt(((proj_tl[0] - proj_bl[0]) ** 2) + ((proj_tl[1] - proj_bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
        [0, 0],
        [maxWidth, 0],
        [maxWidth, maxHeight],
        [0, maxHeight]], dtype=dtype)

    M = cv2.getPerspectiveTransform(dst, proj_pts)

    target_pts = np.asarray(target_pts, dtype=dtype)
    target_pts = np.hstack([target_pts, np.ones([4, 1])])
    target_pts = np.matmul(M, target_pts.T).T
    target_pts[:, 0]/= target_pts[:, 2]
    target_pts[:, 1]/= target_pts[:, 2]
    target_pts = target_pts[:, :2]

    return target_pts
