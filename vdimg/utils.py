import io
import functools


def with_fp(mode='rt'):
    def _with_fp(fn):
        @functools.wraps(fn)
        def decorate(fp, *args, **kwargs):
            if isinstance(fp, str):
                with open(fp, mode=mode) as f:
                    return fn(*args, fp=f, **kwargs)
            elif isinstance(fp, bytes):
                with io.BytesIO(fp) as f:
                    return fn(*args, fp=f, **kwargs)
            else:
                return fn(*args, fp=fp, **kwargs)
        return decorate
    return _with_fp
