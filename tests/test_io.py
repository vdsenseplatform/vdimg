import unittest

import vdimg


class TestImageIo(unittest.TestCase):
    def setUp(self):
        self.img_path = 'tests/data/sample.jpg'

    def test_load_image_path(self):
        img = vdimg.load_img(self.img_path)

    def test_load_image_fp(self):
        with open(self.img_path, 'rb') as f:
            img = vdimg.load_img(f)
    
    def test_load_image_bytes(self):
        with open(self.img_path, 'rb') as f:
            img = vdimg.load_img(f.read())

    def test_image_shape(self):
        size = vdimg.img_shape(self.img_path)
